=====================================================================

                    ZED on IFC6640 DataCollector

=====================================================================

Arquivos importantes:

zed_on_ifc6640_datacollector
  |
  FrameGrabber.py
  |
  SensorVision-uptime.service
  |
  WriteOnStartup.status


=====================================================================

                      Como o pacote funciona?


O script **FrameGrabber.py** funciona em _Python 3_ e depende do
_OpenCV_. Uma vez invocado ele cria um objeto _cv2.VideoCapture_
com a webcam _/dev/video4_ e entra num loop _for_ que captura e grava
quadros na pasta
                            _~/SensorVision/card-storage/zed-capture/_

Para a implementação específica, foi montado em
                                         _~/SensorVision/card-storage_
um dispositivo de armazenamento, _/dev/mmcblk0p1_, que tem cerca de
60GB de espaço disponível formatado em exfat.


Para garantir que a captura de quadros funcione mesmo quando o
processo capota foi criado um serviço associado ao _systemd_,
descrito a seguir:

	SensorVision-uptime.service
		Inicia FrameGrabber.py após a inicialização do alvo
		multi-usuário e é mantido rodando pelo próprio watchdog
		do systemd.

No nosso caso, **ExecStart** do _SensorVision-uptime.service_ inicia
o comando FrameGrabber.py que:

	1- verifica se há espaço em disco
	2- se sim, inicia a captura

Dessa forma, garantimos que existe uma checagem do funcionamento do
código de captura e o seu reinício quando algo vai mal.


=====================================================================

                      Como instalar o pacote?


Seu sistema precisa do _systemd_ para a parte de checagem da operação
mas a instalação do próprio _systemd_ foge ao escopo deste documento.

A única dependência do **FrameGrabber.py** que não vem junto com uma
instalação padrão do Python 3 é o OpenCV que, na IFC6640, precisa ser
compilado para explorar o máximo das instruções **NEON** e **FP16**.

Para registrar o serviço **SensorVision-uptime** com seu _systemd_
basta fazer o seguinte:

	1- cp SensorVision-uptime.service /lib/systemd/system
	2- systemctl daemon-reload
	3- systemctl enable SensorVision-uptime.service
	4- systemctl start SensorVision-uptime

Isso feito o serviço já estará rodando e deverá estar rodando nas
próximas inicializações também.


=====================================================================

                  Com quem falo se algo der errado?


Fale com o Jônathas em +5519981420953 ou em jontzbaker@gmail.com.
No pior dos casos eu não vou saber te ajudar =P
