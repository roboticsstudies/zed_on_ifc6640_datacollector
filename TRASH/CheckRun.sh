#!/bin/bash

echo "Checando espaço em disco disponível..."

if [ `df /dev/mmcblk0p1 | awk '/[0-9]%/ {print $(NF-2)}'` -lt 100000 ]
then
	echo "Temos menos de 100Mb. Parando scripts, serviços e desmontando cartão."
	sudo systemctl stop SensorVision-uptime
	sudo kill `pgrep -x "python3"`
	sudo umount /dev/mmcblk0p1

elif [ `df /dev/mmcblk0p1 | awk '/[0-9]%/ {print $(NF-2)}'` -gt 100000 ]
then
	echo "Achei mais de 100Mb. Checando se o Python 3 está rodando..."
	
	if [ -z `pgrep -x "python3"` ]; then
		echo "Python 3 não está rodando. Iniciando..."
		sudo python3 /home/linaro/SensorVision/zed_on_ifc6640_datacollector/FrameGrabber.py &
		echo "Python 3 iniciado."
	else echo "Python 3 já está rodando. Nenhuma ação necessária."
	fi

else
	echo "Estado inconsistente do sistema. Abortando operações."

fi
