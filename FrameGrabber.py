#!/usr/bin/python3

from datetime import datetime
import subprocess
import cv2


"""
SensorVision
IFC6640 Stereo Camera ZED Recording 

"""

## Starting variables

#counter=0
frame=0
#fail=0
#tentative=0
PATH='/home/linaro/SensorVision/card-storage/zed-capture/' # where we store the frames
ROOT='/home/linaro/SensorVision/zed_on_ifc6640_datacollector/'
DEVICE='/dev/mmcblk0p1'

## Checking recording status

StatusFile = open(ROOT+'WriteOnStartup.status', 'r+')
status = StatusFile.read()
StatusFile.seek(0)
if status [:4] == 'yeah':   # yes and no have both four letters
    recording = 1
elif status [:4] == 'nope': # file is left open during the whole operation!
    recording = 0
else:
    print('SwitchError: unknown WriteOnStartup.status status')


## Initializing camera

zed = cv2.VideoCapture(4)

res = "hd720"          # options "hd1080", "hd720", "fwvga"

if res == "hd1080":
    zed.set(3, 3840)   # set double frame width
    zed.set(4, 1080)   # set single frame height
elif res == "hd720":
    zed.set(3, 2560)
    zed.set(4, 720)
elif res == "fwvga":
    zed.set(3, 1708)
    zed.set(4, 480)

# zed.set(5, 10)       # set frame rate in frames per second
# zed.set(16,0.000050) # sets exposure to 0.000050 seconds per frame
                       # Desaturates frames in very bright conditions


## Capture code proper

while(int(subprocess.Popen(["df", DEVICE], stdout=subprocess.PIPE).communicate()[0].split()[10]) > 50000):
#    input_state = True
#    if input_state == False:
#        counter=counter+1
#        if recording==0:
#            recording=1
#            StatusFile.write('yeah')
#            StatusFile.seek(0)
#        else:
#            recording=0
#            StatusFile.write('nope')
#            StatusFile.seek(0)
#    else:
#         counter=0


    LastFrame = frame
    rval, frame = zed.read()
    if (LastFrame == frame).all():
        #raise BrokenPipeError('Possible broken USB connection caused last two frames to be exactly the same')
        print('BrokenPipeError: possible broken USB connection caused last two frames to be exactly the same')
        fail = fail+1
        if fail > 10:
            rval = False
        elif fail > 100:
            os.system('sudo reboot')
    elif (LastFrame != frame).all():
        fail = 0



    if recording==1 and rval==True:
       filename=PATH+'img-'+str(datetime.now().strftime('%y-%m-%d_%H-%M-%S-%f'))+'.jpg'
       cv2.imwrite(filename,frame)
#       if int(os.path.getsize(filename)) < 20000:
#           raise AttributeError('Possible sensor saturation caused last frame to have less than 20kb')
#           print('AttributeError: possible sensor saturation caused last frame to have less than 20kb')
#       elif fail == 0:
#       cv2.imwrite(filename,frame)
#       out.write(frame)
#       print "Write!"

#    diskfree=int(os.popen('df --output=avail /dev/mmcblk0p2 | sed -n 2p').read())
#    if diskfree<=10240:
#        recording=0
#        zed.release()

zed.release()
